const introduceData = [
    {
      title: "Đa Dạng",
      description: "Cải tiến sản phẩm, chất lượng và trải nghiệm khách hàng luôn là yếu tố tiên quyết trong chiến lược kinh doanh của chúng tôi nhằm thúc đẩy tăng trưởng và trở thành thương hiệu pizza hàng đầu.",
      backgroundColor: "#FFFFCC",
    },
    {
      title: "Chất Lượng",
      description: "Chúng tôi đơn giản hướng đến việc trở thành thương hiệu của mọi nhà, mọi lúc, mọi nơi.",
      backgroundColor: "#FFFF00",
    },
    {
      title:"Hương Vị",
      description:"'Vượt trội' không chỉ là một khái niệm lớn. Thay vào đó, giá trị này là cốt lõi trong mỗi con người của chúng tôi, trong mỗi công việc mà họ làm.",
      backgroundColor:"#FF9966",
    },
    {
      title:"Dịch Vụ",
      description:"Chúng tôi sẵn lòng hỗ trợ vô điều kiện và linh động điều chỉnh tùy theo thử thách để tìm ra giải pháp.",
      backgroundColor:"#FF9900"
    }
                            
  ];
  
  export default introduceData;
  