import React from 'react';

const IntroduceComponent = ({ title, description, backgroundColor }) => {
    return (
        <>
            <div className="col-sm-3 p-3 text-black border" style={{ backgroundColor }}>
                <h3 className="p-2">{title}</h3>
                <p className="p-2">{description}</p>
            </div>
        </>
    );
};

export default IntroduceComponent;
