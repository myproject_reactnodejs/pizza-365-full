import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchComboData, clickSize  } from '../../actions/PizzaOrder.action';

const SizeComponent = ({ apiUrl, bgColor }) => {
    const dispatch = useDispatch();
    const selectedCombo = useSelector((state) => state.PizzaOrderReducer.selectedCombo);
    const comboData = useSelector((state) => state.PizzaOrderReducer.comboData);

    useEffect(() => {
        dispatch(fetchComboData(apiUrl));
    }, [dispatch, apiUrl]);

    // Sử dụng useEffect để log selectedCombo mỗi khi nó thay đổi
    useEffect(() => {
        console.log('Selected Combo:', selectedCombo);
    }, [selectedCombo]);

    const handleSizeSelect = (combo) => {
        dispatch(clickSize(combo)); // Dispatch action clickSize
    };

    return (
        <div className="row">
            {comboData.map((combo, index) => (
                <div className="col-sm-4" key={index}>
                    <div className="card mt-4">
                        <div className={`card-header bg-tab-como-size-${bgColor} text-black text-center`}>
                            <h3>{combo.kichCo}</h3>
                        </div>
                        <div className="card-body text-center">
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item">Đường kính: <b>{combo.duongKinh}</b></li>
                                <li className="list-group-item">Sườn nướng: <b>{combo.suonNuong}</b></li>
                                <li className="list-group-item">Salad: <b>{combo.salad}</b></li>
                                <li className="list-group-item">Nước ngọt: <b>{combo.nuocNgot}</b></li>
                                <li className="list-group-item">
                                    <h1><b>{combo.thanhTien}</b></h1>
                                    <h2>VND</h2>
                                </li>
                            </ul>
                        </div>
                        <div className="card-footer text-center">
                            <button 
                                className={`btn-yellow ${selectedCombo === combo ? 'btn-blue-selected' : ''}`} 
                                onClick={() => handleSizeSelect(combo)}
                            >
                                Chọn
                            </button>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default SizeComponent;
