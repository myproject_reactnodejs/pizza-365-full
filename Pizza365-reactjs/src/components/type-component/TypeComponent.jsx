import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchTypeData, clickType  } from '../../actions/PizzaOrder.action';

const TypeComponent = ({ apiUrl, images }) => {
  const dispatch = useDispatch();
    const selectedType = useSelector((state) => state.PizzaOrderReducer.selectedType);
    const typeData = useSelector((state) => state.PizzaOrderReducer.typeData);

    useEffect(() => {
        dispatch(fetchTypeData(apiUrl));
    }, [dispatch, apiUrl]);

    // Sử dụng useEffect để log selectedType mỗi khi nó thay đổi
    useEffect(() => {
      console.log('Selected Type:', selectedType);
    }, [selectedType]);

    const handleTypeSelect = (type) => {
      dispatch(clickType(type)); // Dispatch action clickSize
    };
    
  return (
    <div className="row">
      {typeData.map((type, index) => (
        <div className="col-sm-4" key={index}>
          <div className="card w-100" style={{ width: '18rem' }}>
            <img src={images[index]} alt={`Pizza ${index + 1}`} className="card-img-top" />
            <div className="card-body card-pizza-type">
              <h4 className="h4-pizza-name">{type.name}</h4>
              <p>{type.loaiPizza}</p>
              <p className="p-pizza-note">{type.note}</p>
              <p className="p-pizza-description">{type.description}</p>
              <p>
              <button 
                className={`btn-yellow ${selectedType === type ? 'btn-blue-selected' : ''}`} 
                onClick={() => handleTypeSelect(type)}
              >
                Chọn
              </button>
              </p>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default TypeComponent;
