import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchDrinkData, selectDrink } from '../../actions/PizzaOrder.action';

const DrinkComponent = ({ apiUrl }) => {
    const dispatch = useDispatch();
    const selectedDrink = useSelector((state) => state.PizzaOrderReducer.selectedDrink);
    const drinkData = useSelector((state) => state.PizzaOrderReducer.drinkData);

    useEffect(() => {
        dispatch(fetchDrinkData(apiUrl)); // Gọi action để fetch dữ liệu nước uống
    }, [dispatch, apiUrl]);

    // Sử dụng useEffect để log selectedDrink mỗi khi nó thay đổi
    useEffect(() => {
        console.log('Selected Drink:', selectedDrink);
      }, [selectedDrink]);

    const handleDrinkSelect = (drink) => {
        dispatch(selectDrink(drink)); // Dispatch action khi người dùng chọn nước uống
    };

    return (
        <div className="form-group">
            <div>
                <label htmlFor="select-drink" style={{ color: "#e0a800", fontSize: "medium" }}>
                    <b>Hãy chọn một loại đồ uống mà bạn thích</b>
                </label>
                <select name="selectDrink" id="select-drink" className="form-control mt-3" onChange={(e) => handleDrinkSelect(e.target.value)}>
                    <option value="NOT_SELECT_DRINK">Tất cả các loại nước uống</option>
                    {drinkData.map((drink) => (
                        <option key={drink.maNuocUong} value={drink.maNuocUong}>{drink.tenNuocUong}</option>
                    ))}
                </select>
            </div>
        </div>
    );
};

export default DrinkComponent;

