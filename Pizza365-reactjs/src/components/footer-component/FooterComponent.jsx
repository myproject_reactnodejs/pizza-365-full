import { Component } from "react";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';
import {
    faFacebook,
    faInstagram,
    faSnapchat,
    faPinterest,
    faTwitter,
    faLinkedin
} from '@fortawesome/free-brands-svg-icons';

class FooterComponent extends Component {
    render() {
        return (
            <>
                <div className="row text-center">
                    <div className="col-sm-12">
                        <h4 className="mt-5">Footer</h4>
                        <a href="#" className="btn bg-main-brand text-white m-3">
                            <FontAwesomeIcon icon={faArrowUp} className='mr-2' />
                            To the top
                        </a>
                        <div className="m-2">
                            <FontAwesomeIcon icon={faFacebook} className="text-main m-1" />
                            <FontAwesomeIcon icon={faInstagram} className="text-main m-1" />
                            <FontAwesomeIcon icon={faSnapchat} className="text-main m-1" />
                            <FontAwesomeIcon icon={faPinterest} className="text-main m-1" />
                            <FontAwesomeIcon icon={faTwitter} className="text-main m-1" />
                            <FontAwesomeIcon icon={faLinkedin} className="text-main m-1" />
                        </div>
                        <div className='mt-4'><strong>Powered By DEVCAMP</strong></div>
                    </div>
                </div>
            </>                 
        )
    }
}
export default FooterComponent;