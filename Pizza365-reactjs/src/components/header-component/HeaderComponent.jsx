import { Component } from "react";
import './HeaderComponent.css'

class HeaderComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMenuOpen: false
        };
    }

    toggleMenu = () => {
        this.setState(prevState => ({
            isMenuOpen: !prevState.isMenuOpen
        }));
    }

    render() {
        return (
            <>
                <div className="row">
                    <div className="col-sm-12">
                        <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-warning">
                            <div className={`collapse navbar-collapse ${this.state.isMenuOpen ? 'show' : ''}`} id="navbarNav">
                                <ul className="navbar-nav nav-fill w-100">
                                    <li className="nav-item active">
                                        <a className="nav-link" href="#">HOME</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#combo">SIZE COMBOS</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#type">PIZZA TYPES</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#contact">FORM CONTACT</a>
                                    </li>
                                </ul>
                            </div>
                            <button className="navbar-toggler" type="button" onClick={this.toggleMenu} aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                        </nav>
                    </div>
                </div>
            </>
        )
    }
}

export default HeaderComponent;