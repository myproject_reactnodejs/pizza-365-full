import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fillFormField } from '../../actions/PizzaOrder.action';
import fields from './fieldData'; // Import mảng fields từ file fields.js

const FormComponent = () => {
    const dispatch = useDispatch();
    // Selectors cac state tu Redux
    const formFields = useSelector((state) => state.PizzaOrderReducer.formFields);
    const selectedCombo = useSelector((state) => state.PizzaOrderReducer.selectedCombo);
    const selectedType = useSelector((state) => state.PizzaOrderReducer.selectedType);
    const selectedDrink = useSelector((state) => state.PizzaOrderReducer.selectedDrink);
    
    // Action fillFormField truy xuat data
    const handleInputChange = (e, fieldName) => {
        const { value } = e.target;
        dispatch(fillFormField(fieldName, value)); 
    };

// Hàm submit order
const handleSubmit = async () => {
    let tienDuocGiam = 0; 
    // Khai báo biến giamGia ở đầu hàm để tránh lỗi "giamGia is not defined"
    // Lấy dữ liệu cuối cùng được chọn cho Combo, Type và Drink, Form info
    // Sử dụng state của redux
    const submittedOrder = {
        comboSize: selectedCombo,
        typePizza: selectedType,
        drink: selectedDrink,
        formFields: formFields 
    };
    console.log('Submitted Order:', submittedOrder);

    // Validate các trường thông tin đã được nhập
    if (!submittedOrder.comboSize || !submittedOrder.typePizza
        || !submittedOrder.drink || !submittedOrder.formFields['input-name'] 
        || !submittedOrder.formFields['input-email'] || !submittedOrder.formFields['input-phone'] 
        || !submittedOrder.formFields['input-address']) {
        alert("Vui lòng chọn combo, type, drink và điền đầy đủ thông tin Order!");
        return; // Ngăn chặn việc thực hiện các lệnh tiếp theo nếu có lỗi
    }
    // Validate định dạng email
    const validateEmail = (email) => {
        const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return re.test(email);
    };

    if (!validateEmail(submittedOrder.formFields['input-email'])) {
        alert("Vui lòng nhập đúng định dạng email!");
        return;
    }
    // Validate phone number in VN
    const validatePhoneNumber = (phoneNumber) => {
        const re = /^(0[0-9]{9,10})$/; // Số điện thoại bắt đầu bằng 0, theo sau bởi 9 hoặc 10 chữ số
        return re.test(phoneNumber);
    };
    
    if (!validatePhoneNumber(submittedOrder.formFields['input-phone'])) {
        alert("Vui lòng nhập đúng định dạng số điện thoại Việt Nam!");
        return;
    }

    // Kiểm tra nếu không có idVoucher
    const voucherId = submittedOrder.formFields['input-voucher-id'];
    if (!voucherId) {
        alert("Bạn không có voucher giảm giá!");
    } else {
        // Gọi API để kiểm tra voucher
        const voucherURL = `http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/${voucherId}`;
        const voucherResponse = await fetch(voucherURL);
        if (!voucherResponse.ok) {
            // Nếu voucher không hợp lệ, thông báo lỗi
            alert("ID voucher không tồn tại!");
            return;
        }

        // Nhận phản hồi từ máy chủ về thông tin voucher
        const voucherData = await voucherResponse.json();

        // Tính toán giảm giá và cập nhật thành tiền
        if (voucherData.phanTramGiamGia) {
            tienDuocGiam = parseFloat(submittedOrder.comboSize.thanhTien.replace(/\./g, '')) * voucherData.phanTramGiamGia / 100;
        }
    }

    // Gửi yêu cầu đặt hàng tới API bằng fetch
    try {
        const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
        const response = await fetch(vBASE_URL, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
            body: JSON.stringify({
                kichCo: submittedOrder.comboSize.kichCo, 
                duongKinh: submittedOrder.comboSize.duongKinh,
                suon: submittedOrder.comboSize.suonNuong,
                salad: submittedOrder.comboSize.salad,
                loaiPizza: submittedOrder.typePizza.loaiPizza,
                idVourcher: submittedOrder.formFields['input-voucher-id'],
                idLoaiNuocUong: submittedOrder.drink,
                soLuongNuoc: submittedOrder.comboSize.nuocNgot,
                hoTen: submittedOrder.formFields['input-name'],
                thanhTien: parseFloat(submittedOrder.comboSize.thanhTien.replace(/\./g, '')) - tienDuocGiam, // Cập nhật nếu có giảm giá
                email: submittedOrder.formFields['input-email'],
                soDienThoai: submittedOrder.formFields['input-phone'],
                diaChi: submittedOrder.formFields['input-address'],
                loiNhan: submittedOrder.formFields['input-message'], 
            })
        });

        if (response.ok) {
            const dataResponse = await response.json();
            console.log(dataResponse);
            alert("Đơn hàng đã được tạo thành công!");
        } else {
            throw new Error(`Error: ${response.status} - ${response.statusText}`);
        }
    } catch (error) {
        console.error(error);
        // Thêm xử lý khi có lỗi, ví dụ: hiển thị thông báo cho người dùng
        alert("Failed to create order. Please try again later.");
    }
};

return (
        <div className="col-sm-12">
            {fields.map((field, index) => ( // Sử dụng mảng fields để render các trường form
                <div key={index} className="form-group mb-4">
                    <label htmlFor={field.id}>{field.label}</label>
                    <input 
                        type={field.type} 
                        className="form-control mt-2" 
                        value={formFields[field.id]} // Lấy giá trị từ Redux state dựa trên id của trường
                        onChange={(e) => handleInputChange(e, field.id)} // Xử lý sự kiện khi trường form thay đổi
                    />
                </div>
            ))}
            <button 
                type="button" 
                className="btn-yellow mt-3" 
                onClick={handleSubmit}
            >
                Gửi
            </button>
        </div>
    );
};

export default FormComponent;
