const fields = [
    { label: "Họ và tên", type: "text", id: "input-name", placeholder: "Họ và tên" },
    { label: "Email", type: "text", id: "input-email", placeholder: "Email" },
    { label: "Điện thoại", type: "text", id: "input-phone", placeholder: "Điện thoại" },
    { label: "Địa chỉ", type: "text", id: "input-address", placeholder: "Địa chỉ" },
    { label: "Lời nhắn", type: "text", id: "input-message", placeholder: "Lời nhắn" },
    { label: "Mã giảm giá (voucher ID)", type: "text", id: "input-voucher-id", placeholder: "Mã voucher..." }
];

export default fields;
