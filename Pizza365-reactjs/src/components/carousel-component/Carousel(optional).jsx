// src/components/CustomCarousel.js
import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Carousel } from 'react-bootstrap';

// Import các hình ảnh
import image1 from '../../assets/images/anh_mon_1.jpg';
import image2 from '../../assets/images/anh_mon_2.jpg';
import image3 from '../../assets/images/anh_mon_3.jpg';
import image4 from '../../assets/images/anh_mon_4.jpg';
import image5 from '../../assets/images/anh_mon_5.jpg';
import image6 from '../../assets/images/anh_mon_6.jpg';
import image7 from '../../assets/images/anh_mon_7.jpg';

const ImageCarousel = () => {
    // State để theo dõi index của ảnh đang hiển thị
    const [activeIndex, setActiveIndex] = useState(0);

    // Mảng chứa các đường dẫn đến ảnh
    const images = [image1, image2, image3, image4, image5, image6, image7];

    // Hàm xử lý chuyển đến ảnh trước
    const handlePrev = () => {
        setActiveIndex((prevIndex) => (prevIndex === 0 ? 6 : prevIndex - 1));
    };

    // Hàm xử lý chuyển đến ảnh sau
    const handleNext = () => {
        setActiveIndex((prevIndex) => (prevIndex === 6 ? 0 : prevIndex + 1));
    };

    // Sử dụng useEffect để tạo chuyển động tự động
    useEffect(() => {
        const intervalId = setInterval(() => {
            handleNext();
        }, 3000); // Thời gian chuyển động, ở đây là 3 giây

        // Cleanup function để ngăn chuyển động khi component unmount
        return () => clearInterval(intervalId);
    }, [activeIndex]);

    // JSX của component, sử dụng Carousel từ react-bootstrap
    return (
        <Carousel activeIndex={activeIndex} onSelect={(index) => setActiveIndex(index)}>
            {images.map((image, index) => (
                <Carousel.Item key={index}>
                    <img
                        className="d-block w-100"
                        src={image}
                        alt={`Slide ${index + 1}`}
                    />
                </Carousel.Item>
            ))}
        </Carousel>
    );
};

export default ImageCarousel;