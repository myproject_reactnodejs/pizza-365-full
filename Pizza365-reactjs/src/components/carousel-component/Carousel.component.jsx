import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './Carousel.component.css';
import { ImNext, ImPrevious } from "react-icons/im";
import image1 from '../../assets/images/anh_mon_1.jpg'; 
import image2 from '../../assets/images/anh_mon_2.jpg'; 
import image3 from '../../assets/images/anh_mon_3.jpg'; 
import image4 from '../../assets/images/anh_mon_4.jpg'; 
import image5 from '../../assets/images/anh_mon_5.jpg'; 
import image6 from '../../assets/images/anh_mon_6.jpg'; 
import image7 from '../../assets/images/anh_mon_7.jpg';


const MySlider = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    nextArrow: <CustomNextArrow />,
    prevArrow: <CustomPrevArrow />,
  };

  return (
    <Slider {...settings}>
      <div>
        <img src={image1} alt="Slide 1" className="d-block w-100" />
      </div>
      <div>
        <img src={image2} alt="Slide 2" className="d-block w-100" />
      </div>
      <div>
        <img src={image3} alt="Slide 3" className="d-block w-100" />
      </div>
      <div>
        <img src={image4} alt="Slide 4" className="d-block w-100" />
      </div>
      <div>
        <img src={image5} alt="Slide 5" className="d-block w-100" />
      </div>
      <div>
        <img src={image6} alt="Slide 6" className="d-block w-100" />
      </div>
      <div>
        <img src={image7} alt="Slide 7" className="d-block w-100" />
      </div>
    </Slider>
  );
};

const CustomNextArrow = (props) => {
  const { onClick } = props;
  return (
    <div className="slick-custom-arrow slick-next-custom" onClick={onClick}>
      <ImNext style={{ color: 'white', fontSize: '40px' }} />
    </div>
  );
};

const CustomPrevArrow = (props) => {
  const { onClick } = props;
  return (
    <div className="slick-custom-arrow slick-prev-custom" onClick={onClick}>
      <ImPrevious style={{ color: 'white', fontSize: '40px' }} />
    </div>
  );
};

export default MySlider;
