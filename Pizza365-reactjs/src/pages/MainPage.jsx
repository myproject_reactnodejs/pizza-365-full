// src/pages/Page.jsx

import React from 'react';
import HeaderComponent from '../components/header-component/HeaderComponent';
import FooterComponent from '../components/footer-component/FooterComponent';
import IntroduceComponent from '../components/introduce-component/IntroduceComponent';
import SizeComponent from '../components/size-component/SizeComponent';
import TypeComponent from '../components/type-component/TypeComponent';
import FormComponent from '../components/form-component/FormComponent';
import CarouselComponent from '../components/carousel-component/Carousel.component';
import DrinkComponent from '../components/drink-component/DrinkComponent';

import introduceData from '../components/introduce-component/introduceData';

import imgHaisan from '../assets/images/pizza_type_hai_san.jpg';
import imgHawai from '../assets/images/pizza_type_hawai.jpg';
import imgBacon from '../assets/images/pizza_type_thit_nuong.jpg';

const MainPage = () => {
  const apiUrl = 'https://647406377de100807b1a4df4.mockapi.io/types'; // api link type pizza
  const images = [imgHaisan, imgHawai, imgBacon];
  return (
    <div>
      {/* header */}
      <div className='container-fluid bg'>
        <HeaderComponent />
      </div>
      {/* content */}
      <div className="container" style={{ padding: "70px 0px 50px 0px" }}>
        <div className="row">
          <div className="col-sm-12">
            <div className="row">
              {/* Title row home */}
              <div className="col-sm-12">
                <h1><b className="text-main">Pizza 365</b></h1>
                <p style={{ fontStyle: "italic", color: '#00628f' }} className="text-second">Truly Italian!</p>
              </div>
              {/* Slide row pizza home */}
              <div className="col-sm-12">
                <div>
                  <CarouselComponent />
                </div>
              </div>
              {/* Title ưu điểm của Quán */}
              <div className="col-sm-12 text-center text-color p-4 mt-4">
                <h2><b className="p-2" style={{ borderBottom: '4px solid #e0a800' }}>Tại sao lại Pizza 365</b></h2>
              </div>
              {/* Content ưu điểm của Quán */}
              <div className="col-sm-12">
                  <div className="row">
                    {introduceData.map((data, index) => (
                      <IntroduceComponent
                        key={index}
                        title={data.title}
                        description={data.description}
                        backgroundColor={data.backgroundColor}
                      />
                    ))}
                  </div>
              </div>
            </div>
            {/* combo */}
            <div id="combo" className="row">
              {/* Title Size Combos */}
              <div className="col-sm-12 text-center text-color p-4 mt-4">
                  <h2><b className="p-2" style={{ borderBottom: '4px solid #e0a800' }}>Chọn Pizza 365</b></h2>
                  <p className='mt-3'>
                    <span className="p-2 text-color">Hãy chọn combo phù hợp với nhu cầu bạn</span>
                  </p>
              </div>
              {/* Content Size Combos */}
              <div className="col-sm-12">
                  <div className="row">
                      {/* Size S */}
                      <SizeComponent 
                          apiUrl="https://647406377de100807b1a4df4.mockapi.io/combos"
                          bgColor={2}  // Passing integer instead of string
                      />
                    </div>
                  </div>
            </div>
            {/* type */}
            <div id="type" className="row">
                {/* Title Pizza Type */}
                <div className="col-sm-12 text-center text-color p-4 mt-4">
                  <h2><b className="p-2" style={{ borderBottom: '4px solid #e0a800' }}>Chọn loại pizza</b></h2>
                </div>
                {/* Pizza Types */}
                <div className="col-sm-12">
                    {/* OCEAN MANIA */}
                    <TypeComponent apiUrl={apiUrl} images={images} />   
                </div>
            </div>
            {/* chọn đồ uống */}
            <div className="row">
                <div className="col-sm-12 text-center text-color p-2 mt-4">
                  <h2><b className="p-2" style={{ borderBottom: '4px solid #e0a800' }}>Chọn đồ uống</b></h2>
                  <p className='mt-3'><span className="p-2">Hãy chọn đồ uống phù hợp với bạn</span></p>
                </div>
                <div className="col-sm-12">
                  <DrinkComponent 
                    apiUrl="http://203.171.20.210:8080/devcamp-pizza365/drinks"
                  />
                </div>
            </div>
            {/* thông tin đơn hàng */}
            <div id="contact" className="row">
              {/* Title Form Contact Us */}
                <div className="col-sm-12 text-center text-color p-4 mt-4">
                  <h2><b className="p-2" style={{ borderBottom: '4px solid #e0a800' }}>Gửi đơn hàng</b></h2>
                </div>
                {/* Content Form Contact Us */}
                <div className="col-sm-12 p-2 jumbotron">
                  <div className="row">
                    <div className="col-sm-12">
                      <FormComponent />
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      {/* footer */}
      <div className="container-fluid bg-warning p-3">
        <FooterComponent />
      </div>
    </div>
  );
}

export default MainPage;
