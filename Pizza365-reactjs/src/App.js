// src/App.js
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import './App.css';

import MainPage from './pages/MainPage'; // Import Page.jsx

const App = () => {
  return (
    <MainPage /> // Render Page component
  );
}

export default App;
