// src/reducers/PizzaOrder.reducer.js
import { combineReducers } from 'redux';
import * as ActionTypes from '../constants/PizzaOrder.constant';

// API Combo size
const comboDataReducer = (state = [], action) => {
  switch (action.type) {
    case ActionTypes.FETCH_COMBO_DATA_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};
const comboErrorReducer = (state = null, action) => {
  switch (action.type) {
    case ActionTypes.FETCH_COMBO_DATA_FAILURE:
      return action.payload;
    default:
      return state;
  }
};
// API Type size
const typeDataReducer = (state = [], action) => {
  switch (action.type) {
    case ActionTypes.FETCH_TYPE_DATA_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};
const typeErrorReducer = (state = null, action) => {
  switch (action.type) {
    case ActionTypes.FETCH_TYPE_DATA_FAILURE:
      return action.payload;
    default:
      return state;
  }
};
// API Type Drink
const drinkDataReducer = (state = [], action) => {
  switch (action.type) {
    case ActionTypes.FETCH_DRINK_DATA_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};
const drinkErrorReducer = (state = null, action) => {
  switch (action.type) {
    case ActionTypes.FETCH_DRINK_DATA_FAILURE:
      return action.payload;
    default:
      return state;
  }
};

const clickSizeReducer = (state = null, action) => {
  switch (action.type) {
    case ActionTypes.CLICK_SIZE:
      return action.payload;
    default:  
      return state;
  }
};

const clickTypeReducer = (state = null, action) => {
  switch (action.type) {
    case ActionTypes.CLICK_TYPE:
      return action.payload;
    default:
      return state;
  }
};

const selectedDrinkReducer = (state = null, action) => {
  switch (action.type) {
    case ActionTypes.SELECT_DRINK:
      return action.payload;
    default:
      return state;
  }
};

const fillFormFieldsReducer = (state = {}, action) => {
  switch (action.type) {
    case ActionTypes.FILL_FORM_FIELD:
      return {
        ...state,
        [action.payload.fieldName]: action.payload.value,
      };
    default:
      return state;
  }
};

const pizzaOrderReducer = combineReducers({
  comboData: comboDataReducer,
  comboError: comboErrorReducer,
  typeData: typeDataReducer,
  typeError: typeErrorReducer,
  drinkData: drinkDataReducer,
  drinkError: drinkErrorReducer,
  selectedCombo: clickSizeReducer,
  selectedType: clickTypeReducer,
  selectedDrink: selectedDrinkReducer,
  formFields: fillFormFieldsReducer,
});

export default pizzaOrderReducer;
