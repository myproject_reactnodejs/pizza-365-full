// src/reducers/index.js
import { combineReducers } from 'redux';
import PizzaOrderReducer from './PizzaOrder.reducer';

const rootReducer = combineReducers({
  PizzaOrderReducer,
});

export default rootReducer;

