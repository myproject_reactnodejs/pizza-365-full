// src/actions/PizzaOrder.action.js
import axios from 'axios';
import * as ActionTypes from '../constants/PizzaOrder.constant';

// Call api combo pizza
export const fetchComboData = (apiUrl) => {
  return async (dispatch) => {
    try {
      const response = await axios.get(apiUrl);
      dispatch({
        type: ActionTypes.FETCH_COMBO_DATA_SUCCESS,
        payload: response.data,
      });
    } catch (error) {
      dispatch({
        type: ActionTypes.FETCH_COMBO_DATA_FAILURE,
        payload: error.message,
      });
    }
  };
};
// Call api type pizza
export const fetchTypeData = (apiUrl) => {
  return async (dispatch) => {
    try {
      const response = await axios.get(apiUrl);
      dispatch({
        type: ActionTypes.FETCH_TYPE_DATA_SUCCESS,
        payload: response.data,
      });
    } catch (error) {
      dispatch({
        type: ActionTypes.FETCH_TYPE_DATA_FAILURE,
        payload: error.message,
      });
    }
  };
};
// Call api Drink
export const fetchDrinkData = (apiUrl) => {
  return async (dispatch) => {
    try {
      const response = await axios.get(apiUrl);
      dispatch({
        type: ActionTypes.FETCH_DRINK_DATA_SUCCESS,
        payload: response.data,
      });
    } catch (error) {
      dispatch({
        type: ActionTypes.FETCH_DRINK_DATA_FAILURE,
        payload: error.message,
      });
    }
  };
};
// export const clickSize = (combo) => {
//   return (dispatch, getState) => {
//     // Thực hiện dispatch action và sau đó log selectedCombo
//     dispatch({
//       type: ActionTypes.CLICK_SIZE,
//       payload: combo,
//     });
//     const { selectedCombo } = getState().PizzaOrderReducer;
//     console.log('Selected Combo:', selectedCombo);
//   };
// }; cach 2 dispatch truc action

export const clickSize = (combo) => ({
  type: ActionTypes.CLICK_SIZE,
  payload: combo,
});

export const clickType = (type) => ({
  type: ActionTypes.CLICK_TYPE,
  payload: type,
});

export const selectDrink = (drink) => ({
  type: ActionTypes.SELECT_DRINK,
  payload: drink,
});

export const fillFormField = (fieldName, value) => ({
  type: ActionTypes.FILL_FORM_FIELD,
  payload: { fieldName, value },
});


