// src/actions/apiActions.js
import axios from 'axios';

export const fetchDataSuccess = (type, data) => ({
  type,
  payload: data,
});

export const fetchDataFailure = (type, error) => ({
  type,
  payload: error,
});

export const fetchData = (apiUrl, successType, failureType) => {
  return async (dispatch) => {
    try {
      const response = await axios.get(apiUrl);
      dispatch(fetchDataSuccess(successType, response.data));
    } catch (error) {
      dispatch(fetchDataFailure(failureType, error.message));
    }
  };
};