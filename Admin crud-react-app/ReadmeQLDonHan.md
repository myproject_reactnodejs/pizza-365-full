Quản Lý Đơn Hàng

1. Giải thích code các method đã viết

Trong dự án này, chúng tôi đã viết một số method để thực hiện các chức năng cốt lõi của hệ thống:

createOrder: Method này được sử dụng để thêm một đơn hàng mới vào hệ thống.
updateOrder (by ID) : Method này được sử dụng để cập nhật trạng thái của một đơn hàng có sẵn trong hệ thống dựa trên orderCode.
removeOrder (by ID): Method này được sử dụng để xóa một đơn hàng khỏi hệ thống dựa trên ID.
getOrderByCode: Method này được sử dụng để lấy thông tin chi tiết của một đơn hàng dựa trên orderCode.
getAllOrders: Method này được sử dụng để lấy tất cả các đơn hàng có trong hệ thống hiển thị ra UI.

2. Mô tả dự án
Dự án Quản Lý Đơn Hàng là một ứng dụng web giúp quản lý các đơn hàng, từ việc thêm mới, chỉnh sửa đến xóa và xem chi tiết các đơn hàng đã tạo. Nó cung cấp một giao diện thân thiện và dễ sử dụng, giúp người dùng quản lý và theo dõi tình trạng của các đơn hàng một cách hiệu quả.

3. Các chức năng (có ảnh chụp)

Thêm Đơn Hàng Mới

Chi Tiết/Chỉnh sửa Đơn Hàng

Quản Lý Danh Sách Đơn Hàng

4. Công nghệ sử dụng
Dự án được xây dựng bằng các công nghệ sau:

Frontend:
ReactJS: ReactJS được sử dụng làm nền tảng cho phần giao diện người dùng (UI) của dự án. ReactJS cung cấp một cách tiếp cận linh hoạt và dễ dàng để xây dựng các ứng dụng web hiệu quả.
Material-UI: Material-UI là một UI framework được xây dựng trên cơ sở ReactJS, cung cấp các thành phần UI tiêu chuẩn và thiết kế theo nguyên tắc Material Design của Google, giúp tạo ra giao diện đẹp và dễ sử dụng.
HTML, CSS: HTML và CSS được sử dụng để tạo ra cấu trúc và thiết kế giao diện người dùng của ứng dụng.

Công nghệ khác:
State Management (Redux): Redux được sử dụng để quản lý trạng thái của ứng dụng trên phía client, giúp quản lý trạng thái toàn cục một cách hiệu quả và dễ dàng.
Hooks: React Hooks được sử dụng để quản lý trạng thái và logic trong các component của ứng dụng React, giúp việc viết code trở nên ngắn gọn và dễ hiểu hơn.

Backend:
APIs:
Order list: http://203.171.20.210:8080/devcamp-pizza365/orders
Drink list: http://203.171.20.210:8080/devcamp-pizza365/drinks

