import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Orderlist from './Component/Order';
import { Provider } from 'react-redux';
import store from './Redux/Store';
import { ToastContainer } from 'react-toastify';

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Orderlist />} /> {/* Simplified syntax for Route */}
        </Routes>
      </BrowserRouter>
      <ToastContainer position='top-right'></ToastContainer>
    </Provider>
  );
}

export default App;
