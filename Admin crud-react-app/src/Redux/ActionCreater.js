import axios from "axios";
import { AddRequest, 
    RemoveRequest, 
    UpdateRequest, 
    getAllRequestFail, 
    getAllRequestSuccess, 
    getbycodeSuccess, 
    makeRequest } from "./Action";
import { toast } from "react-toastify";

const baseURL = "http://203.171.20.210:8080/devcamp-pizza365/orders";

export const getAllOrders = () => {
    return (dispatch) => {
        dispatch(makeRequest());
        setTimeout(() => {
            axios.get(baseURL)
                .then(res => {
                    const _list = res.data;
                    dispatch(getAllRequestSuccess(_list));
                })
                .catch(err => {
                    dispatch(getAllRequestFail(err.message));
                });
        }, 800);
    };
};

export const getOrderByCode = (orderCode) => {
    return (dispatch) => {
        axios.get(`${baseURL}/${orderCode}`)
            .then(res => {
                const _obj = res.data;
                dispatch(getbycodeSuccess(_obj));
            })
            .catch(err => {
                toast.error('Failed to fetch the data');
            });
    };
};

export const createOrder = (data) => {
    return (dispatch) => {
        axios.post(baseURL, data)
            .then(res => {
                dispatch(AddRequest(data));
                toast.success('Order created successfully.');
            })
            .catch(err => {
                toast.error('Failed to create Order due to :' + err.message);
            });
    };
};

export const updateOrder = (id, data) => {
    return (dispatch) => {
        axios.put(`${baseURL}/${id}`, data)
            .then(res => {
                dispatch(UpdateRequest(data));
                toast.success('Order updated successfully.');
            })
            .catch(err => {
                toast.error('Failed to update Order due to :' + err.message);
            });
    };
};


export const removeOrder = (id) => {
    return (dispatch) => {
        axios.delete(`${baseURL}/${id}`)
            .then(res => {
                dispatch(RemoveRequest(id));
                toast.success('Order Removed successfully.');
            })
            .catch(err => {
                toast.error('Failed to remove Order due to :' + err.message);
            });
    };
};
