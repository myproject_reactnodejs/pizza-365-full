import { MAKE_REQ, 
    OPEN_POPUP, 
    REQ_ADD_SUCC, 
    REQ_DELETE_SUCC, 
    REQ_GETALL_FAIL, 
    REQ_GETALL_SUCC, 
    REQ_GETBYCODE_SUCC, 
    REQ_UPDATE_SUCC } from "./ActionType"

export const initialstate = {
    isLoading: false,
    orderList: [],
    orderObj: {},
    errormessage: ''
}

export const OrderReducer = (state = initialstate, action) => {
    switch (action.type) {
        case MAKE_REQ:
            return {
                ...state,
                isLoading: true
            }

        case REQ_GETALL_SUCC:
            return {
                ...state,
                isLoading: false,
                orderList: action.payload
            }

        case REQ_GETBYCODE_SUCC:
            return {
                ...state,
                orderObj: action.payload
            }
            
        case REQ_GETALL_FAIL:
            return {
                ...state,
                isLoading: false,
                orderList: [],
                errormessage: action.payload
            }
        case OPEN_POPUP:
            return {
                ...state,
                orderObj: {}
            }
        case REQ_ADD_SUCC:
            const newOrder = action.payload; // Dữ liệu đơn hàng mới được trả về từ API
            return {
                ...state,
                orderList: [...state.orderList, newOrder] // Thêm đơn hàng mới vào danh sách đơn hàng hiện tại
            };
        case REQ_UPDATE_SUCC:
            const _data = { ...action.payload };
            const _finaldata = state.orderList.map(item => {
                return item.id === _data.id ? _data : item
            });
            return {
                ...state,
                orderList: _finaldata
            }
        case REQ_DELETE_SUCC:
            const _filterdata = state.orderList.filter((data) => {
                return data.id !== action.payload
            })
            return {    
                ...state,
                orderList: _filterdata
            }
        default: return state;
    }
}