import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { OrderReducer } from "./Reducer";
import { thunk } from 'redux-thunk'; // Correct import for thunk middleware
import logger from "redux-logger";

const rootReducer = combineReducers({
  order: OrderReducer
});

const middleware = [thunk, logger]; // Define middleware as an array

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(middleware) // Combine default middleware with custom middleware
});

export default store;
