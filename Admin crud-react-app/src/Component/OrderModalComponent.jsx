import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import { FormHelperText } from '@mui/material';

const BASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/drinks";

const OrderModalComponent = ({
    open,
    handleClosePopup,
    title,
    handleSubmit,
    kichCo,
    setKichCo,
    duongKinh,
    setDuongKinh,
    suon,
    setSuon,
    salad,
    setSalad,
    soLuongNuoc,
    setSoLuongNuoc,
    thanhTien,
    setThanhTien,
    loaiPizza,
    setLoaiPizza,
    idVourcher,
    setIdVourcher,
    idLoaiNuocUong,
    setIdLoaiNuocUong,
    hoTen,
    setHoTen,
    email,
    setEmail,
    soDienThoai,
    setSoDienThoai,
    diaChi,
    setDiaChi,
    loiNhan,
    setLoiNhan,
    trangThai,
    setTrangThai,
    agreeTerm,
    setAgreeTerm,
    isEdit
    }) => {
    
    const [loaiNuocUongList, setLoaiNuocUongList] = useState([]); // State lay ds drink tu API
    
    useEffect(() => {
            // Gọi API để lấy danh sách loại nước uống khi component được mount
            axios.get(BASE_URL)
                .then(response => {
                    setLoaiNuocUongList(response.data);
                })
                .catch(error => {
                    console.error('Error fetching loai nuoc uong list:', error);
                });
        }, [setLoaiNuocUongList]); 

    // Tai su dung cac statex
    useEffect(() => {
            // Update details based on selected pizza size
            if (kichCo === 'S') {
                setDuongKinh('20');
                setSuon('2');
                setSalad('200');
                setSoLuongNuoc('2');
                setThanhTien('150000');
            } else if (kichCo === 'M') {
                setDuongKinh('25');
                setSuon('4');
                setSalad('300');
                setSoLuongNuoc('3');
                setThanhTien('200000');
            } else if (kichCo === 'L') {
                setDuongKinh('30');
                setSuon('8');
                setSalad('500');
                setSoLuongNuoc('4');
                setThanhTien('250000');
            }
        }, [kichCo]);

    return (
        <Dialog open={open} onClose={handleClosePopup} fullWidth maxWidth="md">
            <DialogTitle sx={{ backgroundColor:'#e0a800'}}>
                <span>{title}</span>
                <IconButton style={{ float: 'right' }} onClick={handleClosePopup}><CloseIcon color="primary" /></IconButton>
            </DialogTitle>
            <DialogContent sx={{ mt:2 }}>
                <form onSubmit={handleSubmit}>
                    <Stack spacing={2} margin={2}>
                        {/* Selec Size */}
                        <FormControl fullWidth error={kichCo === ''}>
                            <InputLabel>Kich Co</InputLabel>
                            <Select
                                required
                                value={kichCo}
                                onChange={(e) => setKichCo(e.target.value)}
                                variant="outlined"
                                label="Kich Co"
                            >   
                                <MenuItem value="S">S</MenuItem>
                                <MenuItem value="M">M</MenuItem>
                                <MenuItem value="L">L</MenuItem>
                            </Select>
                            {kichCo === '' && <FormHelperText sx={{ fontStyle: 'italic' }}>Ban phai chon kich co (*)</FormHelperText>}
                        </FormControl>
                        <TextField
                            required
                            error={duongKinh.length === 0}
                            value={duongKinh}
                            onChange={(e) => setDuongKinh(e.target.value)}
                            variant="outlined"
                            label="Duong Kinh"
                            disabled={kichCo !== ''}
                        />
                        <TextField
                            required
                            error={suon.length === 0}
                            value={suon}
                            onChange={(e) => setSuon(e.target.value)}
                            variant="outlined"
                            label="Suon"
                            disabled={kichCo !== ''}
                        />
                        <TextField
                            required
                            error={salad.length === 0}
                            value={salad}
                            onChange={(e) => setSalad(e.target.value)}
                            variant="outlined"
                            label="Salad"
                            disabled={kichCo !== ''}
                        />
                        <TextField
                            required
                            error={soLuongNuoc.length === 0}
                            value={soLuongNuoc}
                            onChange={(e) => setSoLuongNuoc(e.target.value)}
                            variant="outlined"
                            label="So Luong Nuoc"
                            disabled={kichCo !== ''}
                        />
                        <TextField
                            required
                            error={thanhTien.length === 0}
                            value={thanhTien}
                            onChange={(e) => setThanhTien(e.target.value)}
                            variant="outlined"
                            label="Thanh Tien"
                            disabled={kichCo !== ''}
                        />
                        <FormControl fullWidth error={loaiPizza === ''}>
                            <InputLabel>Loai Pizza</InputLabel>
                            <Select
                                required
                                value={loaiPizza}
                                onChange={(e) => setLoaiPizza(e.target.value)}
                                variant="outlined"
                                label="Loai Pizza"
                            >
                                <MenuItem value="Seafood">Seafood</MenuItem>
                                <MenuItem value="Hawaii">Hawaii</MenuItem>
                                <MenuItem value="Bacon">Bacon</MenuItem>
                            </Select>
                            {loaiPizza === '' && <FormHelperText sx={{ fontStyle: 'italic' }}>You must select a pizza type (*)</FormHelperText>}
                        </FormControl>
                        <TextField type="number" value={idVourcher} onChange={(e) => setIdVourcher(e.target.value)} variant="outlined" label="ID Voucher" />
                        {/* Selec Drink */}
                        <FormControl fullWidth error={idLoaiNuocUong === ''}>
                            <InputLabel>Chon Do Uong</InputLabel>
                            <Select
                                required
                                label="Chon Do Uong"
                                value={idLoaiNuocUong}
                                onChange={(e) => setIdLoaiNuocUong(e.target.value)}
                                disabled={isEdit} // Khoá Select khi đang ở chế độ chỉnh sửa
                            >
                                <MenuItem disabled value="">
                                    <em>Chon mot loai nuoc uong</em>
                                </MenuItem>
                                {/* Sử dụng idLoaiNuocUong để map và tạo các tùy chọn */}
                                {Array.isArray(loaiNuocUongList) && loaiNuocUongList.map((drink) => (
                                    <MenuItem key={drink.maNuocUong} value={drink.maNuocUong} disabled={isEdit}>
                                        {drink.tenNuocUong}
                                    </MenuItem>
                                ))}
                            </Select>
                            {idLoaiNuocUong === '' && 
                            <FormHelperText sx={{ fontStyle: 'italic' }}>Bạn phải chọn một loại nước uống (*)</FormHelperText>}
                        </FormControl>
                        <TextField required error={hoTen.length === 0} value={hoTen} onChange={(e) => setHoTen(e.target.value)} variant="outlined" label="Ho Ten" />                      
                        <FormControl fullWidth>
                            <TextField
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                variant="outlined"
                                placeholder='exp@gmail.com'
                                label="Email"
                                type='email'
                            />
                            <FormHelperText>Please enter a valid email address</FormHelperText>
                        </FormControl>
                        <TextField required error={soDienThoai.length === 0} value={soDienThoai} onChange={(e) => setSoDienThoai(e.target.value)} variant="outlined" label="So Dien Thoai" />
                        <TextField required error={diaChi.length === 0} multiline maxRows={2} minRows={2} value={diaChi} onChange={(e) => setDiaChi(e.target.value)} variant="outlined" label="Dia Chi" />
                        <TextField type='text' value={loiNhan} onChange={(e) => setLoiNhan(e.target.value)} variant="outlined" label="Loi Nhan" />
                        <RadioGroup value={trangThai} onChange={(e) => setTrangThai(e.target.value)} row>
                            <FormControlLabel value="open" control={<Radio />} label="Open" />
                            <FormControlLabel value="cancel" control={<Radio />} label="Cancel" />
                            <FormControlLabel value="confirmed" control={<Radio />} label="Confirmed" />
                        </RadioGroup>
                        <FormControlLabel checked={agreeTerm} onChange={(e) => setAgreeTerm(e.target.checked)} control={<Checkbox />} label="I agree to the terms and conditions" />
                        <Button disabled={!agreeTerm} variant="contained" type="submit">Submit</Button>
                    </Stack>
                </form>
            </DialogContent>
        </Dialog>
    );
};

export default OrderModalComponent;
