import React from 'react';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import TableContainer from '@mui/material/TableContainer';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import IconButton from '@mui/material/IconButton';
import EditNoteSharpIcon from '@mui/icons-material/EditNoteSharp';
import DeleteForeverSharpIcon from '@mui/icons-material/DeleteForeverSharp';
import TablePagination from '@mui/material/TablePagination';

const OrderListComponent = ({ 
    columns, 
    orderState, 
    handleAdd, 
    handleEdit, 
    handleRemove, 
    handlePageChange, 
    handleRowPerPageChange, 
    page, 
    rowPerPage,
    counter }) => {
    
    return (
        <Paper sx={{ margin: '1%' }}>
            <h2 style={{ textAlign: 'center', color: 'midnightblue' }}>PIZZA-365 QUAN LY DON HANG</h2>
            <div style={{ margin: '1%' }}>
                <Button onClick={handleAdd} variant="contained">Add New (+)</Button>
            </div>
            <div style={{ margin: '1%' }}>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow style={{ backgroundColor: '#e0a800' }}>
                                {columns.map((column) =>
                                    <TableCell
                                        key={column.id}
                                        style={{ color: 'midnightblue', textAlign: 'center', fontWeight: 'bold' }}
                                    >
                                        {column.name}
                                    </TableCell>
                                )}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {orderState.orderList &&
                                orderState.orderList
                                    .slice(page * rowPerPage, page * rowPerPage + rowPerPage)
                                    .map((row, i) => {
                                        return (
                                            <TableRow key={i}>
                                                <TableCell>{counter + i}</TableCell>
                                                <TableCell>{row.id}</TableCell>
                                                <TableCell>{row.orderCode}</TableCell>
                                                <TableCell>{row.kichCo}</TableCell>
                                                <TableCell>{row.duongKinh}</TableCell>
                                                <TableCell>{row.suon}</TableCell>
                                                <TableCell>{row.salad}</TableCell>
                                                <TableCell>{row.loaiPizza}</TableCell>
                                                <TableCell>{row.idVourcher}</TableCell>
                                                <TableCell>{row.thanhTien}</TableCell>
                                                <TableCell>{row.giamGia}</TableCell>
                                                <TableCell>{row.idLoaiNuocUong}</TableCell>
                                                <TableCell>{row.soLuongNuoc}</TableCell>
                                                <TableCell>{row.hoTen}</TableCell>
                                                <TableCell>{row.email}</TableCell>
                                                <TableCell>{row.soDienThoai}</TableCell>
                                                <TableCell>{row.diaChi}</TableCell>
                                                <TableCell>{row.loiNhan}</TableCell>
                                                <TableCell>{row.trangThai}</TableCell>
                                                <TableCell>{row.ngayTao}</TableCell>
                                                <TableCell>{row.ngayCapNhat}</TableCell>
                                                <TableCell style={{ display: 'flex', justifyContent: 'space-between' }}>
                                                    <IconButton
                                                        onClick={() => handleEdit(row.orderCode)}
                                                        style={{ backgroundColor: '#00628f' }}
                                                    >
                                                        <EditNoteSharpIcon style={{ fontSize: 28, color: '#e0a800' }} />
                                                    </IconButton>
                                                    <IconButton
                                                        onClick={() => handleRemove(row.id)}
                                                        style={{ backgroundColor: '#e0a800' }}
                                                    >
                                                        <DeleteForeverSharpIcon style={{ fontSize: 28, color: 'red' }} />
                                                    </IconButton>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[2, 4, 6, 9]}
                    rowsPerPage={rowPerPage}
                    page={page}
                    count={orderState.orderList?.length || 0}
                    component="div"
                    onPageChange={handlePageChange}
                    onRowsPerPageChange={handleRowPerPageChange}
                />
            </div>
        </Paper>
    );
};

export default OrderListComponent;
