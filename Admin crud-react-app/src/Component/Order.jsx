// Import các component và hook cần thiết từ thư viện MUI và React
import { useEffect, useState } from "react";
import { createOrder, getAllOrders, getOrderByCode, removeOrder, updateOrder } from "../Redux/ActionCreater";
import { connect, useDispatch, useSelector } from "react-redux";
import { OpenPopup } from "../Redux/Action";
import OrderModalComponent from './OrderModalComponent'; // Import OrderModalComponent
import OrderListComponent from './OrderListComponent'; // Import OrderListComponent

const Order = ({ loadOrder, orderState }) => {
    // Khai báo các cột trong bảng
    const columns = [
        { id: 'stt', name: 'STT' },
        { id: 'id', name: 'ID' },
        { id: 'orderCode', name: 'Order Code' },
        { id: 'kichCo', name: 'Kich Co' },
        { id: 'duongKinh', name: 'Duong Kinh' },
        { id: 'suon', name: 'Suon' },
        { id: 'salad', name: 'Salad' },
        { id: 'loaiPizza', name: 'Loai Pizza' },
        { id: 'idVoucher', name: 'ID Voucher' },
        { id: 'thanhTien', name: 'Thanh Tien' },
        { id: 'giamGia', name: 'Giam Gia' },
        { id: 'idLoaiNuocUong', name: 'Nuoc Uong' },
        { id: 'soLuongNuocUong', name: 'SL Nuoc' },
        { id: 'hoTen', name: 'Ho Ten' },
        { id: 'email', name: 'Email' },
        { id: 'soDienThoai', name: 'Phone' },
        { id: 'diaChi', name: 'Dia Chi' },
        { id: 'loiNhan', name: 'Loi Nhan' },
        { id: 'trangThai', name: 'Trang Thai' },
        { id: 'ngayTao', name: 'Create Date' },
        { id: 'ngayCapNhat', name: 'Update Date' },
        { id: 'action', name: 'Action' }
    ];

    // Sử dụng hook useDispatch từ Redux
    const dispatch = useDispatch();

    // Khai báo state cho các trường thông tin đơn hàng 
    const [kichCo, setKichCo] = useState('');
    const [duongKinh, setDuongKinh] = useState('');
    const [suon, setSuon] = useState('');
    const [salad, setSalad] = useState(''); 
    const [soLuongNuoc, setSoLuongNuoc] = useState('');
    const [thanhTien, setThanhTien] = useState('');
    const [loaiPizza, setLoaiPizza] = useState('');
    const [idVourcher, setIdVourcher] = useState('');
    const [idLoaiNuocUong, setIdLoaiNuocUong] = useState('');
    const [hoTen, setHoTen] = useState(''); 
    const [email, setEmail] = useState('');
    const [soDienThoai, setSoDienThoai] = useState(''); 
    const [diaChi, setDiaChi] = useState(''); 
    const [loiNhan, setLoiNhan] = useState(''); 
    const [trangThai, setTrangThai] = useState('open'); 
    const [agreeTerm, setAgreeTerm] = useState(false);
    const [open, setOpen] = useState(false);

    // state phan trang
    const [rowPerPage, setRowPerPage] = useState(4); // mac dinh phan tu hien thi tren trang
    const [page, setPage] = useState(0);

    // Khai báo state cho việc chỉnh sửa đơn hàng
    const [isEdit, setIsEdit] = useState(false);
    const [title, setTitle] = useState('Create Order');

    // State để đếm số lượng đơn hàng
    const [counter, setCounter] = useState(1);

    // Sử dụng useSelector để lấy đối tượng đơn hàng cần chỉnh sửa
    const editObj = useSelector((state) => state.order.orderObj);

    // Khi component mount, nếu có đối tượng đơn hàng cần chỉnh sửa, set các trường thông tin cho state tương ứng
    useEffect(() => {
        if (editObj && Object.keys(editObj).length > 0) {
            setKichCo(editObj.kichCo || ''); 
            setDuongKinh(editObj.duongKinh || ''); 
            setSuon(editObj.suon || ''); 
            setSalad(editObj.salad || ''); 
            setSoLuongNuoc(editObj.soLuongNuoc || ''); 
            setThanhTien(editObj.thanhTien || ''); 
            setLoaiPizza(editObj.loaiPizza || ''); 
            setIdVourcher(editObj.idVourcher || ''); 
            setIdLoaiNuocUong(editObj.idLoaiNuocUong); 
            setHoTen(editObj.hoTen || ''); 
            setEmail(editObj.email || '');
            setSoDienThoai(editObj.soDienThoai || ''); 
            setDiaChi(editObj.diaChi || ''); 
            setLoiNhan(editObj.loiNhan || ''); 
            setTrangThai(editObj.trangThai || 'open'); 
        } else {
            clearState();
        }
    }, [editObj]);
    
     // Hàm thay đổi trang
    const handlePageChange = (event, newPage) => {
        setPage(newPage);
    };

    // Hàm thay đổi số hàng trên mỗi trang
    const handleRowPerPageChange = (event) => {
        setRowPerPage(+event.target.value);
        setPage(0);
    };

    const handleAdd = () => {
        setIsEdit(false);
        setTitle('Create New Order');
        openPopup();
    };

    const handleClosePopup = () => {
        setOpen(false);
    };
    // Hàm mở form thêm hoặc chỉnh sửa đơn hàng
    const openPopup = () => {
        setOpen(true);
        clearState();
        dispatch(OpenPopup());
    };

    const handleEdit = (orderCode) => {
        setIsEdit(true);
        setTitle('Update Order');
        setOpen(true);
        dispatch(getOrderByCode(orderCode));
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        // Lấy thông tin từ state
        const _obj = {
            kichCo, 
            duongKinh, 
            suon, 
            salad, 
            soLuongNuoc, 
            thanhTien, 
            loaiPizza, 
            idVourcher, 
            idLoaiNuocUong, 
            hoTen, 
            email,
            soDienThoai, 
            diaChi, 
            loiNhan, 
            trangThai 
        };
        // Nếu đang chỉnh sửa, gọi action updateOrder, ngược lại gọi action createOrder
        if (isEdit) {
            dispatch(updateOrder(editObj.id, _obj));
        } else {
            dispatch(createOrder(_obj));
            setCounter(counter + 1);
        }
        setTimeout(() => {
            handleClosePopup();
            // Làm mới trang sau khi đã hiển thị toast và chờ một khoảng thời gian
            window.location.reload();
        }, 2000); // Đợi 1.6s
    };
    
    const handleRemove = (id) => {
        if (window.confirm('Do you want to remove?')) {
            dispatch(removeOrder(id));
        }
    };

    // Hàm clear state của form
    const clearState = () => {
        setKichCo(''); 
        setDuongKinh(''); 
        setSuon(''); 
        setSalad(''); 
        setSoLuongNuoc(''); 
        setThanhTien('');
        setLoaiPizza(''); 
        setIdVourcher('');
        setIdLoaiNuocUong(''); 
        setHoTen('');
        setDiaChi(''); 
        setSoDienThoai(''); 
        setLoiNhan(''); 
        setTrangThai('open'); 
        setAgreeTerm(false);
    };

    // Khi component mount, gọi action loadOrder để lấy danh sách đơn hàng
    useEffect(() => {
        loadOrder();
    }, [loadOrder]);

    // Trả về giao diện
    return (
        orderState.isLoading ? <div><h2>Loading.....</h2></div> :
        orderState.errorMessage ? <div><h2>{orderState.errorMessage}</h2></div> :
            <div>
            {/* Order List page */}
            <OrderListComponent
                columns={columns}
                orderState={orderState}
                handleAdd={handleAdd}
                handleEdit={handleEdit}
                handleRemove={handleRemove}
                handlePageChange={handlePageChange}
                handleRowPerPageChange={handleRowPerPageChange}
                page={page}
                rowPerPage={rowPerPage}
                counter={counter}
            />
            {/* Modal Order */}
            <OrderModalComponent
                open={open}
                handleClosePopup={handleClosePopup}
                title={title}
                handleSubmit={handleSubmit}
                kichCo={kichCo}
                setKichCo={setKichCo}
                duongKinh={duongKinh}
                setDuongKinh={setDuongKinh}
                suon={suon}
                setSuon={setSuon}
                salad={salad}
                setSalad={setSalad}
                soLuongNuoc={soLuongNuoc}
                setSoLuongNuoc={setSoLuongNuoc}
                thanhTien={thanhTien}
                setThanhTien={setThanhTien}
                loaiPizza={loaiPizza}
                setLoaiPizza={setLoaiPizza}
                idVourcher={idVourcher}
                setIdVourcher={setIdVourcher}
                idLoaiNuocUong={idLoaiNuocUong}
                setIdLoaiNuocUong={setIdLoaiNuocUong}
                hoTen={hoTen}
                setHoTen={setHoTen}
                email={email}
                setEmail={setEmail}
                soDienThoai={soDienThoai}
                setSoDienThoai={setSoDienThoai}
                diaChi={diaChi}
                setDiaChi={setDiaChi}
                loiNhan={loiNhan}
                setLoiNhan={setLoiNhan}
                trangThai={trangThai}
                setTrangThai={setTrangThai}
                agreeTerm={agreeTerm}
                setAgreeTerm={setAgreeTerm}
                isEdit={isEdit}
            />
        </div>
        
    );
};
// Ánh xạ Redux state vào props
const mapStateToProps = (state) => {
    return {
        // Trạng thái của Redux được gán vào prop 'orderstate'
        orderState: state.order
    };
};
// Ánh xạ các hành động Redux vào props
const mapDispatchToProps = (dispatch) => {
    return {
         // Hành động 'GetAllOrders' được gọi và dispatch khi load order được gọi
        loadOrder: () => dispatch(getAllOrders())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Order);
